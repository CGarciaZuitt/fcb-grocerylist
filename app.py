from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# # static route
# @app.route('/')
# def index():
# 	return 'This is the Home Page "/"'

# @app.route('/Tinay')
# def greet():
# 	return "Good day Tinay! How are you?"

# #  dynamic route
# @app.route('/hello/<name>')
# def hello(name):
# 	return f'Hello, {name}'

# @app.route('/post/<int:post_id>')
# def show_post(post_id):
# 	return f'Post {post_id}'

# mock database
groceryList = ["Chicken"]

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html", groceryList=groceryList, enumerate=enumerate)

@app.route("/add_groceryItem", methods=["POST"])
def add_groceryItem():
    groceryItem = request.form["item"]
    groceryList.append(groceryItem)
    return redirect(url_for("index"))

@app.route("/delete_groceryItem/<int:item_index>", methods=["POST"])
def delete_groceryItem(item_index):
    groceryList.pop(item_index)
    return redirect(url_for("index"))



if __name__ == "__main__" :
	app.run(debug = True)


